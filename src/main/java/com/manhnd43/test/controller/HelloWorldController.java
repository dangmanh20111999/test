package com.manhnd43.test.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/test")
public class HelloWorldController {

	@GetMapping
	public String HelloWorld() {
		return "Nguyen Dang Manh Do Thu Tra!";
	}
}
